import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from "./components/user/register/register.component";
import { DashboardComponent } from "./components/user/dashboard/dashboard.component";

const app_routes: Routes = [
  { path: 'home', component: RegisterComponent },
  { path: 'dashboard', component: DashboardComponent},
  { path: '**', pathMatch: 'full', redirectTo: 'home' }
];

export const app_routing = RouterModule.forRoot(app_routes);
import { Component, OnInit } from '@angular/core';
import { UserService} from "../../../services/user.service";
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  user:any [] = [];
  constructor(private _user:UserService) {
    const user = {
      username: localStorage.getItem('user'),
      password: localStorage.getItem('password')
    }
    _user.login(user).subscribe(data=> this.user = data.data[0])
   }

  ngOnInit() {
  }

}

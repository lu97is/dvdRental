import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { isDevMode } from '@angular/core';
import 'rxjs/add/operator/map';
@Injectable()
export class UserService {
  domain: String;
  user:String;
  password:String;
  constructor(private http: Http) {
    if (isDevMode()) {
      this.domain = 'http://localhost:8080/';

    } else {
      //  this.domain = 'http://45.55.133.179:8080';
      this.domain = 'https://devtomato.herokuapp.com';
      // this.domain = 'http://localhost:8080';

    }
  }

  getProduct() {
    return this.http.get(this.domain + 'api/actors').map(res => res.json());
  }
  getCities() {
    return this.http.get(this.domain + 'api/cities').map(res => res.json());
  }
  getCountries(){
    return this.http.get(this.domain + 'api/countries').map(res => res.json());
  }
  postCustomer(user) {
    return this.http.post(this.domain + 'api/customer', user).map(res => res.json());
  }
  postAddress(add) {
    return this.http.post(this.domain + 'api/address', add).map(res => res.json());
  }
  login(user){
    return this.http.post(this.domain + 'api/login', user).map(res => res.json());
  }
  storeData(user, password) {
    localStorage.setItem('user', user);
    localStorage.setItem('password', password);
    this.user = user;
    this.password = password;
  }
}
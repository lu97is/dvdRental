'use strict'
const promise = require('bluebird');
const options = {
    promiseLib: promise
};

const pgp = require('pg-promise')(options);
const connectionString = 'postgres://postgres:eagle1997@localhost:5432/DvdRental'
const db = pgp(connectionString);

module.exports = (router) => {
    //get
    router.get('/actors', (req, res, next) => {
        db.query('select * from actor')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data

                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })
    router.get('/cities', (req, res, next) => {
        db.query('select * from city')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    });
    router.get('/countries', (req, res, next) => {
        db.query('select * from country')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    });
    //post
    router.post('/actor', (req, res, next) => {
        db.none('insert into actor(first_name,last_name,last_update)' +
                'values(${first_name}, ${last_name}, ${last_update})',
                req.body)
            .then(function () {
                res.status(200)
                    .json({
                        status: 'success',
                        message: 'Inserted one puppy'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    });
    router.post('/customer', (req, res, next) => {
        db.none('insert into customer(username,password,first_name,last_name,address_id,email,store_id,activebool,create_date,last_update,active)' +
            'values(${username}, ${password}, ${first_name}, ${last_name},${address_id}, ${email}, ${store_id},${activebool},${create_date},${last_update},${active})',
            req.body).then(() => {
            res.status(200).json({
                status: 'success',
                message: 'User register'
            })
        }).catch((err) => {
            return next(err);
        })
    })
    router.post('/login', (req,res,next)=>{
        db.query('select * from customer where username = $1 and password = $2', [req.body.username, req.body.password])
        .then((data) => {
            res.status(200).json({
                status: 'success',
                data: data
            })
        }).catch((err) => {
            return next(err);
        })
    })
    router.post('/address', (req, res, next) => {
        db.query('insert into address(address,address2,district,city_id,postal_code,phone,last_update)' +
                'values(${address},${address2},${district},${city_id},${postal_code},${phone},${last_update})' +
                'RETURNING address_id', req.body)
            .then((data) => {
                res.status(200).json({
                    status: 'success',
                    message: data[0].address_id
                })
            }).catch((err) => {
                return next(err);
            })
    })
    //put
    router.put('/actor/:id', (req, res, next) => {
        db.none('update actor set first_name=$1, last_name=$2, last_update=$3 where actor_id=$4', [req.body.first_name, req.body.last_name, req.body.last_update, parseInt(req.params.id)])
            .then(function () {
                res.status(200)
                    .json({
                        status: 'success',
                        message: 'Updated puppy'
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })
    //delete
    router.delete('/actor/:id', (req, res, next) => {
        db.result('delete from actor where actor_id = $1', req.params.id)
            .then(function (result) {
                res.status(200)
                    .json({
                        status: 'success',
                        message: `Removed ${result.rowCount} puppy`
                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })
    return router;
}
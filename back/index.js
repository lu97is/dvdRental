const express = require('express');
const app = express();
const router = express.Router();
const pg = require('pg');
const config = require('./config/database');
const port = 8080 || process.env.PORT
const path = require('path')
const api = require('./routes/api')(router);

const cors = require('cors')
const bodyParser = require('body-parser');
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(express.static(__dirname + '/public'))
app.use('/api', api);
app.listen(port, ()=>{
    console.log('Conected on port ' + port)
})

